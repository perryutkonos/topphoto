'use strict';

var webpack = require('webpack')
module.exports = {

    watch: true,
    devtool: "cheap-module-source-map",
    watchOptions: {
        aggregateTimeout: 100
    },
    entry: "./src/js/index",

    output: {
        path: __dirname + '/build/js',
        filename: 'bundle.js'
    },
    module: { //Обновлено
        loaders: [ //добавили babel-loader
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    "presets": ["es2015", "stage-0", "react"],
                    "plugins": [
                        ["babel-root-import", {
                            "rootPathSuffix": "src/js"
                        }]
                    ]
                }
            }
        ],
        //noParse: /\/node_modules\/(jquery\/dist\/jquery)/
    },
   plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            "root.jQuery": "jquery",
            Hammer: "hammerjs/hammer"
        })
    ]
}